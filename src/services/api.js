import axios from 'axios';

export default (Vue) => {
    Vue.mixin({
        beforeCreate() {
            const options = this.$options;
            if (options.userService) {
                this.$userService = options.userService;
            } else if (options.parent && options.parent.$userService) {
                this.$userService = options.parent.$userService;
            }
        },
    });
};
