import axios from 'axios';

export default {
    install(Vue) {
        const api = axios.create({
            baseURL: 'https://api.covid19api.com/',
            timeout: 10000,
        });
        Vue.api = Vue.prototype.$api = api
    }
}
